//Odilon Bertrand Niyomugabo 
//CSE 002-210
//9/11/2018
// Arithmetic 

public class Arithmetic{
  // Main purpose of this prgram is to calculate the total 
  // cost of a purchases - pants, shirts, belts - and calculate
  // their individual sales taxes and make their sum and make sums of 
  // total of transactions and purchases made before tax. 
  public static void main ( String args []){
   // variable declaration 
    
    double totalCostOfPants;  //total cost of pants    
    double totalCostOfShirts; // total cost of shirts 
    double totalCostOfBelts; //total cost of belts
    double totalCostofpurchasesbftax;  //total cost of the entire purchase before tax
    double totalSalesTax; //determining the total sales taxes
    double totalcostoftransactions; //calculating the total cost of the purchase included the tax  
    
// declaring and initializing  assumptions made  
    int numPants = 3; // number of pants 
    double pantsPrice = 34.98;  // cost of one pant 
    int numShirts= 2;  // number of shirts 
    double shirtPrice = 24.99;  // cost of one shirt
    int numBelts = 1;  // number of belts 
    double beltPrice = 33.99;  // cost of one belt 
    double paSalesTax = 0.06; // the pa tax rate  
    
  
  totalCostOfPants = numPants*pantsPrice; //calculating the total cost of pants using the number of paints times the price of the pants
  totalCostOfShirts = numShirts*shirtPrice; //calculating the total cost of shirts using the number of shirts times the price of the shirts
  totalCostOfBelts = numBelts*beltPrice; //calculating the total cost of belts using the number of belts times the price of the belts
  
  totalCostofpurchasesbftax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;//calculating the price of the purchase before taxes
  
  totalSalesTax = (totalCostofpurchasesbftax * paSalesTax); //this is the total sales tax is the total cost of the purchase times the sales tax
  totalSalesTax = ((int)(totalSalesTax*100))/100.0; // rounding it to decimal avoiding unncessary figures
  
  totalcostoftransactions = totalCostofpurchasesbftax + totalSalesTax; //the total cost of the transactions with taxes is equalt to the total cost of the purchase plus the sales tax
   
  
  System.out.println("The total cost of the pants purchased without tax is: $" + totalCostOfPants); //printing the total cost of the pants
  System.out.println("The tax of pants is: $" + ((int)(totalCostOfPants * paSalesTax *100))/100.0); //printing the total cost of the pants with tax
  System.out.println("The total cost of the shirts purchased without tax is: $" + totalCostOfShirts); //printing the total cost of the shirts 
  System.out.println("The tax of shirts is: $" +  ((int)(totalCostOfShirts * paSalesTax *100))/100.0); //printing the total cost of the shirts with tax
  System.out.println("The total cost of the bets without tax is: $" + totalCostOfBelts);  //printing the total cost of the belts
  System.out.println("The tax of belts is: $" + ((int)(totalCostOfBelts * paSalesTax *100))/100.0);  //printing the total cost of the belts with tax
  System.out.println("The total cost of the purchase before taxes is: $" + totalCostofpurchasesbftax); //printing the cost of the purchase before sales tax 
  System.out.println("The total sales tax is: $" + totalSalesTax); //printing the cost of the sales tax from the total cost of the purchase  
  System.out.println("Thte total cost of the purchase with tax is: $" + totalcostoftransactions); //printing the total cost of the purchase including the sales tax
    
     }//end of main method  
} //end of class 
    
  