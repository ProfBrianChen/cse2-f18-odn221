// Odilon Bertrand Niyomugabo
// Sept 5, 2018 
// CSE 002- 210
// Cyclometer.java: program that prints 
//the number of minutes for each trip, print the number of counts for each trip,
//distance of each trip in miles, and print the 
// for the two trips combined 

public class Cyclometer { 
  public static void main ( String  [] args ) {
    
    int secsTrip1 = 480; //declaring seconds for the first trip 
    int secsTrip2 = 3220; // declaring seconds for the second trip 
    int countsTrip1= 1561; // declaring   number of counts for trip 1
    int countsTrip2 = 9037; //declaring numbere of counts for trip 2 
    
    double wheelDiameter = 27.0, 
  PI = 3.14159,
    feetPerMile = 5280,
    inchesPerFoot = 12, 
    secondsPerMinute = 60; // declaring variables and assigning them with their values 
    double distanceTrip1, distanceTrip2, totalDistance; // declaring distances 
    
    System.out.println ( "Trip 1 took " + ( secsTrip1/secondsPerMinute)+ 
                        " minutes and had " + countsTrip1 + " counts."); // command that prints out trips minutes and counts 1
    System.out.println ( "Trip 2 took " + ( secsTrip2/secondsPerMinute)+ 
                        " minutes and had " + countsTrip2 + " counts."); //command that prints out trips minutes and counts 2 
  
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // formula to calculate the distance of the first trip 
    distanceTrip1 = inchesPerFoot*feetPerMile; // code that givees distance in miles 
    distanceTrip2 = countsTrip2 * wheelDiameter * PI/ inchesPerFoot/ feetPerMile; // formula to calculate distance for the second trip 
    totalDistance = distanceTrip1 + distanceTrip2; // formula to calculat the total distance 
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles "); // printing out the results of the distance of the first trip
    System.out.println("Trip 2 was " + distanceTrip2 + " miles "); // printing out the results fo the distance of the second trip
    System.out.println( "The total distance was " + totalDistance + " miles"); // printing the results of the total distance
    
  }
}
