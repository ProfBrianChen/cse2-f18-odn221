//Odilon Bertrand Niyomugabo 
// odn221
// November 13, 2018 
// Hw 08
// Program that shuffles cards, and asks the user how many cards they want to pick, and then asks them if they wanna keep enjoying the movies 

import java.util.Random; // importing the random class
import java.util.Scanner;//import Scanner class
public class hw08{ 
 public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); 
  //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};//array for suits   
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};//array for digits 
  String[] cards = new String[52];//array for the cards
  String[] hand = new String[5];//array for the hand
  int numCards = 7;//the number of cards wanted in a hand 
  int again = 1;//prompt for user 
  int index = 51;//index for the deck
  for (int i=0; i<52; i++){//assiging the cards to the deck 
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
  } 
  System.out.println();
  printArray(cards);//printing the deck 
  shuffle (cards);//shuffling the cards
  System.out.println("\n\nShuffled:");
  printArray(cards);//printing the shuffled deck 
  while(again == 1){//while loop to get another hand when asked by user
  if(numCards > index + 1){//if number of Cards is greater than the deck creating and shuffling new deck
   for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
     }
    shuffle(cards);
     index = 51;
  } 
     hand = getHand(cards,index,numCards);//getting cards in a hand
     System.out.println("Hand:"); 
     printArray(hand);//printing the hand
     index = index - numCards;//changing index
     System.out.println("\n \nEnter a 1 if you want another hand drawn");//prompting user for response 
     again = scan.nextInt(); //taking in user response
  }  
   }//end of main method
public static void shuffle( String[] list ){ 
 
  String index ="";
  
  Random ran= new Random (); // this will help us in assigning random numbers
 
  for (int i = 0; i< list.length ; i++){
 int n = ran.nextInt (52); // generating numbers to shuffle from 
  
list [0] = index; 
index = list [n]; 
list [n] = list [0];

}
} // end of shuffle method                        
public static String[] getHand ( String [] list, int index, int numCards){ 
  
  System.out.print("\n \n How many cards do you want:");
  
  Scanner scanner = new Scanner (System.in); // declaring the scanner that will help asking the user if he/she wants to keep enjoying the majic 
  
  numCards = scanner.nextInt();
  
  String [] pickedlength = new String[numCards]; // assigning the 
  
  for ( int i =0; i < pickedlength.length; i++){  // picking the cards from the deck to enter into the hand
  
 pickedlength[i] =list[index] ;
 
index = index -1;  
  }
  return pickedlength; 
} // end of get hand method 
public static void printArray (String[] list){ 
  
  for ( int i =0; i < list.length; i++){ // loop that will help in printing the numbers  
    System.out.print (list [i] +" " ); 
  } 
} // end of print array method
}// end of class