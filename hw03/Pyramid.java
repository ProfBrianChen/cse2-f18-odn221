// Odilon Bertrand Niyomugabo 
// Lab 003 09/17/2018 

import java.util.Scanner;

public class Pyramid{ 

// program that is going to calculate the volume of a pyramid 
  
public  static void main ( String [] args ) { 

 Scanner myScanner = new Scanner ( System.in ); 
  
  System.out.println (" Enter the following data: "); 
  
  System.out.print (" The square Side of the pyramid is: "); 
  
  // command that is going to take the input of the length of the pyramid
  double length = myScanner.nextDouble (); 
  
  System.out.print ( " The height of the pyramid is: "); 
  
  // command that is going to take the input of the height of the pyramid
  double height = myScanner.nextDouble(); 
  
  
 // command to calculate the volume of the pyramid 
 double volume = (Math.pow (length, 2) * height )/3; 
  
  System.out.print ("The volume inside the pyramid is: " + volume + "."); 
  
} // end of the main method
} // end of the class 

  