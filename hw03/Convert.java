// Odilon Bertrand Niyomugabo 
// Lab 003 09/17/2018 



import java.util.Scanner;

public class Convert{ 

  // program that will convert rain in a certain affected area 
  // into put into cubic miles
  
public  static void main ( String [] args ) { 

 Scanner myScanner = new Scanner ( System.in); 
 
 System.out.print ( " Enter the affected area in acres:");
  // command that will the input - affected area in acres - as required
  double affectedarea = myScanner.nextDouble ();
  
  System.out.print (" Enter the rainfall in the affected area in inches: "); 
   
  // command that will tkae the rainfall in the affected are
  double affectedarearainfall = myScanner.nextDouble(); 
 
  // command that will convert area affected and rain and put them into gallons. 
  double affectedgallons = affectedarea * affectedarearainfall; 
  
  // command that will calculate the rain in cubic miles from gallons 
  double quantityofrain = affectedgallons * 27154.2857 * 9.08169e-13;   
  
  System.out.println ( quantityofrain + " cubic miles"); 
  
} // end of main method 
} // end of the class 
  
  
  
  