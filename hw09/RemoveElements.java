// Odilon Bertrand Niyomugabo 
// CSE 2 Hw 09 - RemoveElements.java 
// program that will shuffle a random array of 10 elements and then look for an index required by the user and the remove it from the program.
// if the index is found, if not found, it is going to say not found and print the same old array. And then we wil ask the user for a target avalue 
// after the user gives it, we are going to delete it from the array, and then bring the original array. If the target was not found, we will 
// provide the original one. And then we will give an option to the user either to terminate the code or run it again. 

import java.util.Scanner; //scan method
import java.util.Random; //random method
public class RemoveElements{
  
public static void main(String [] arg) {
Scanner scan = new Scanner(System.in);
int num[] = new int[10];

int newArray1[];
int newArray2[];
int index, target;
String answer="";
do{
System.out.print("Random input 10 ints [0-9]");
num = randomInput();
String out = "";
out += listArray(num);
System.out.println(out);
System.out.print("Enter the index ");
index = scan.nextInt();
newArray1 = delete(num,index); // calling the delete method 
String out1="The output array is ";
out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}" System.out.println(out1);
System.out.print("Enter the target value ");//prompting user to input the target element
    target = scan.nextInt();//taking in input for the target element
    newArray2 = remove(num,target);//calling remove method
    String out2="The output array is ";//printing the result of remove method
    out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
    System.out.println(out2);//printing the result of remove method
     
    System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
    answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
}

public static String listArray(int num[]){//method to print out the arrays
 String out="{";//printing "{"
 for(int j=0;j<num.length;j++){//for loop to print each element in the array
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";//printing "}"
 return out;//returnig the String of the printed array
}
public static int[] randomInput () {
Random rand = new Random ();
int[]array = new int [10]; //making the new required array
for (int i = 0; i < 10; i++) {
array[i] = (int) (Math. random() * 9 + 1);
}
return array;
}
public static int[] delete(int list[], int pos) { //create a new array that has one member fewer than list, and be composed 
  //of all of the same members except the member in the position pos.  
int []array = new int [list.length-1];
if (pos > list.length-1 || pos < 0) {
System.out.println ("The index is not valid."); 
System.out.print ("The output array is ");
for (int i = 0; i < list.length; i++) {
System.out.print (list[i] + " ");
}
System.out.println ("");
return list;
}
for (int i = 0; i < pos; i++) {
array[i] = list[i];
}
for(int i = pos;i < array.length; i++){
array[i] = list[i+1];
}
System.out.print ("The output array is "); 
for (int i = 0; i < array.length; i++) {
System.out.print (array[i] + " ");
}
System.out.println ("");

return array;
}

 public static int[] remove(int[] list, int target){//method to remove specific elements
   int count = 0;//declaring and initializing the count for total number of elements we are looking for
   int count2 = 0;//declaring and initializing the count for shift in index
   for(int i=0;i<list.length;++i){
    if(target == list[i]){
     count++;
   }
   }
   if(count == 0){//if no occurences print error message
    System.out.println("Element " + target + " was not found");
    return list;//return original array
   }
   System.out.println("Element " + target + " has been found");
   
   int[] notRandomArray = new int[list.length-count];//declaring new array depending on the number of occurences
   for(int i=0;i<list.length;++i){//going through the original list for the removal and formation of new array
    if(target == list[i]){//if the target element is found
     count2++;//shift the index
     continue;//go back to the loop
    }
     notRandomArray[i-count2] = list[i];//make the new array depending on the shift on index
   }
   return notRandomArray; //returning the new array
  }//end of remove method
   
} // end of class 

