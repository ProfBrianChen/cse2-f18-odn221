// Odilon Bertrand Niyomugabo 
// CSE 2 Hw 09 - CSE2Linear.java 
// program that takes an array of 15 integers with range from  0 - 100 from the user. And then printh them 
//And then do a binary  search to look for the desired number by the user and then say where it was found. 
// After that, scramble  randomly the set of those 15 integers, and then do a linear search for the desired 
// user numbers and  then say where it was found and how many iterations it took. 


import java.util.Scanner; // scanner method 
import java.util.Random; // random method 
public class CSE2Linear{
  public static void main (String [] args){ // main method 
 
    int [] finalgrades = new int [15];  // assgning 15 to the array 
Scanner scr = new Scanner (System.in);  // scanner 

System.out.println ("Enter 15 ascending final grades for CSE 2 students:"); 

for (int i=0; i<15; i++){ 
  
  boolean gradeisInt = false; 
  
  while (!gradeisInt){ 
    gradeisInt = scr.hasNextInt(); 
    
    if (gradeisInt == true) {
      finalgrades[i] = scr.nextInt(); 
    }
    else { 
      System.out.println ("Sorry, you did not type an integer, please do type another one");  
    }
    
    if (finalgrades [i] > 100 || finalgrades [i] <0){ 
      System.out.println ("Sorry, your entered integer is not in the 0-100 range. Try again"); 
    gradeisInt = false; 
    }
    if (i>0 && (finalgrades [i] < finalgrades [i-1])){ 
      System.out.println ("Sorry, the number you entered should be greater or equal to the last one. Try again");
    gradeisInt = false; 
    }
  }}

for (int i=0; i <15; i++){
  System.out.print (finalgrades [i]+ " "); 
} 
    System.out.println ("\n\n Enter a grade to search for (using binary search) :"); 
    
    int grade = 0;
    boolean gradeisInt = false; 
    
  while (!gradeisInt){ 
    gradeisInt = scr.hasNextInt(); 
    
    if (gradeisInt == true) {
     grade = scr.nextInt(); 
    }
    else { 
      System.out.println ("Sorry, you did not type an integer, please do type another one");  
      scr.next(); 
    }
    
    if (grade > 100 || grade <0){ 
      System.out.println ("Sorry, your entered integer is not in the 0-100 range. Try again"); 
    gradeisInt = false; 
    }
    }
  
  
  CSE2Linear.binarySearch ( finalgrades, finalgrades.length, grade); //calling the binary search array 
  
  System.out.println ("Scrambled:"); 
  
  CSE2Linear.scrambledarray (finalgrades); // calling the scrambled array 
  
  for (int i=0; i < 15; i++){
    System.out.print (finalgrades [i] + " "); 
  }
  
  System.out.println (" \n\n Enter a grade to search for: (using linear search)"); 
  
  grade =0;
  gradeisInt = false;
    
  while (!gradeisInt){ 
    gradeisInt = scr.hasNextInt(); 
    
    if (gradeisInt == true) {
      grade = scr.nextInt(); 
    }
    else { 
      System.out.println ("Sorry, you did not type an integer, please do type another one");  
      scr.next();
    }
    
    if (grade> 100 || grade <0){ 
      System.out.println ("Sorry, your entered integer is not in the 0-100 range. Try again"); 
    gradeisInt = false; 
    }
  }
  
  CSE2Linear. linearSearch (finalgrades, grade); // calling the linear search method 

  } // end of the main method 

  public static void binarySearch ( int numbers [], int numbers_size, int key){ // this method will be doing a binary search for a specific
    // target number of the user choice and then say how many iterations it took to find it if found it. And say it didn't find it. 
    
    int mid;  int low; int high; 
    int count =0;
    boolean gradeisInt = false; 
    
    low = 0;
    high = numbers_size-1; 
    
    while (high >= low){ 
      mid = (low + high)/2; 
      if (numbers[mid] < key) { 
        low = mid +1; 
      }
      
      else if (numbers[mid] > key){ 
        high = mid - 1; 
      } 
      
      else { 
        
        System.out.println ( key +" was found in the list with "+ count + " iterations"); 
       gradeisInt = true; 
        break;
      }
      count ++; 
    }
    if (gradeisInt == false ){
      System.out.println (key + " was not found in the list with " + count + " iterations"); 
    } 
 } // end of the binary search method 
  
  
  public static void scrambledarray (int[] grades){ // this method will help in scrambling the array entered by the user 
 
Random rand = new Random(); // Random number generator

for (int i=0; i<grades.length; i++) {
int randomPosition = rand.nextInt(grades.length);
int temp = grades[i];
grades[i] = grades[randomPosition];
grades[randomPosition] = temp;
}
  
  } // end of the scrambledarray 
  
  public static void linearSearch (int finalgrades [], int grade ){ // method will be doing a linear search of an element of the user choice 
    // then say where it is found and how many iterations it took to arrive on finding it and say it didn't find it otherwise. 
    
    boolean gradeisInt = false; 
    
    int count =0; 
    for (int i = 0; i < finalgrades.length; i++) {
if (finalgrades [i] == grade ) {
System.out.println (grade + " was found in the list with " + count + " iterations");
gradeisInt = true;
break;
}
count ++;
}
if (gradeisInt == false) {
System.out.println (grade  + " was not found in the list with " + count + " iterations");
}
  } // end of linear search method 

  } // end of class 