//Odilon Bertrand Niyomugabo 
// CSE 002 - 210 
// Program that will require user to input a text and give them a menu full of options to pick from 
// which will manupulate their data 

import java.util.Scanner; // import the scanner 
public class hw07{ // opening my class 
  
  public static void main (String [] args){ // main method that will help in calling methods from the menu option 
    
   String userInput = sampleText (); 
   char userOption  = printMenu (); 
    
    while (userOption!= 'q') {
 while (!((userOption == 'c') || (userOption == 'w') || (userOption == 'f') || (userOption == 'r') || (userOption == 's') || (userOption == 'q'))){
  userOption = printMenu();
  }
 if (userOption == 'c') {
  System.out.println("Number of non-white space characters is: " + getNumOfNonWSCharacters(userInput));
 }
 else if (userOption == 'w') {
  System.out.println(("Number of words is: " + getNumOfWords(userInput)));
 }
 else if (userOption == 'f') {
  Scanner myScanner = new Scanner(System.in);
  System.out.println ("Enter the word or phrase to be found: ");
  String userInput2 = myScanner.nextLine();
  System.out.println ( "\"" + userInput2 + "\"" + " " + "instances: " + findText(userInput2, userInput));
 }
 else if (userOption == 'r') {
  System.out.println (replaceExclamation(userInput));
 }
 else if (userOption == 's') {
  System.out.println (shortenSpace(userInput));
 }
 userOption = printMenu();
}
}
 public static String sampleText(){ // declaring method sample text which will help in taking userinput as a sample text
    
    Scanner myScanner  = new Scanner (System.in); 
    
    System.out.println("Enter a sample text"); 
    
    String userInput = myScanner.nextLine(); 
    
    System.out.println ("You entered:"+userInput);
    
    return userInput; 
    
  } 

public static char printMenu() { // this is the method that will have the menu to help meet the needs of the user 
 Scanner myScanner = new Scanner(System.in);
 System.out.println ("\nMENU\n\r\n" + 
   "c - Number of non-whitespace characters\r\n" + 
   "w - Number of words\r\n" + 
   "f - Find text\r\n" + 
   "r - Replace all !'s\r\n" + 
   "s - Shorten spaces\r\n" + 
   "q - Quit\r\n" + 
   " \r\n" + 
   "Choose an option:\r\n" + 
   "");
 String userInput = myScanner.next();
 char userOption = userInput.charAt(0);
 
 return userOption;
 }

public static int getNumOfNonWSCharacters (String userOption) { // method that will count the number of non-white space characters
 int count = 0;
 for (int i = 0; i < userOption.length(); i++) {
   if (!(Character.isWhitespace(userOption.charAt(i)))) 
   {
     count++;
   }
 }
 return count;
}

public static int getNumOfWords( String userOption) { // method that will get the number of words
 int wordCount = 0;

    boolean word = false;
    int lineedn = userOption.length() - 1;

    for (int i = 0; i < userOption.length(); i++) {
        if (Character.isLetter(userOption.charAt(i)) && i != lineedn) {
            word = true; 
        } else if (!Character.isLetter(userOption.charAt(i)) && word) {
            wordCount++;
            word = false;
                  } else if (Character.isLetter(userOption.charAt(i)) && i == lineedn) {
            wordCount++;
        }
    }
    return wordCount;
} 

public static int findText (String testWord, String userInput) { // method that will help in finding a word or phra
       int count=0;
       String []s1=userInput.split(" ");
   
       for(int i=0;i<s1.length;i++)
       {
          if(s1[i].equals(testWord))
           {
               count++; 
           }
       }
       return count;
   }

public static String replaceExclamation (String userInput) { // method that is going to replace the exclamation points in the code   
 int count = userInput.length();
 String testString = "";
 int j = 0;
 for ( int i = 0; i < count; i++ ) {
  if (userInput.charAt(i) == '!') {
   testString += userInput.substring(j, i) + "." ;
   j = i + 1;
  }
   }
 testString += userInput.substring(j);
 return testString;
}
public static String shortenSpace (String userInput) {  // this  method will shorten space that means getting rid of unncessary ones    
 int count = userInput.length();
 String testString = "";
 int j = 1;
 for ( int i = 0; i < count - 1; i++ ) {
  if (userInput.charAt(i) == ' ') {
   testString += userInput.substring(j-1, i) + " " ;
   int k = i + 1;
   while (userInput.charAt(k) == ' ') {
    j = j + 1;
    k = k + 1;
    i = i + 1;
   }
   i=k;
   j=i+1;
  }
  
   }
 testString += userInput.substring(j-1);
 return testString;
} }
    