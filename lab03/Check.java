// Odilon Bertrand Niyomugabo 
// Lab 003 09/12/2018 

// Program that helps friends who have gone 
// out to know what each individual is going to pay 

import java.util.Scanner;

public class Check{ 

public  static void main ( String [] args ) { 

 Scanner myScanner = new Scanner ( System.in ); 
 
  
 System.out.print ( " Enter the original cost of the check in the form of xx.xx:");
  
  double checkCost = myScanner.nextDouble (); 
  
 System.out.print (" Enter the percentage tip that you wish to pay as a whole number ( in the from xx ): ");
double tipPercent = myScanner.nextDouble (); 
  
  tipPercent /= 100;  // converting the percentae into a decimal value 
    
    System.out.print ( "Enter the numbe rof people who went out to dinner: ");
  int numPeople = myScanner.nextInt ();
  
  double totalCost; 
  double costPerPerson; 
  
  int dollars, dimes, pennies; // used for storing digits with the same decimal points
  
  totalCost = checkCost * ( 1 + tipPercent); 
  costPerPerson = totalCost / numPeople; 
  
  dollars = (int) costPerPerson; // getting the whole amount, dropping decimal fraction
  
  dimes = ( int ) ( costPerPerson * 10) % 10; // getting the amount of dimes
  pennies = (int) ( costPerPerson *100 ) % 10; 
  System.out.println ( "Each persion in the group owes $" + dollars + '.' + dimes + pennies); 
  
} // ending the main directory
} // ending the class 