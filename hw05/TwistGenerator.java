//Odilon Bertrand Niyomugabo 
// CSE 2 - 210
// October 11, 2018
// Twist Generator
// This is a program that is supposed to generate twists that are responsive to the user input. 

import java.util.Scanner; // importing the scanner
public class TwistGenerator{  // declaring our class and renaming Twist Generator  
public static void main(String [] args){ // declaring the main body
// declaring variables that will be used in the program; 
  String junk; 
  int length =1; 
  
  
 Scanner userInput = new Scanner (System.in); // renaming and declaring the scanner 
  
  System.out.println ("Please input a positive number for the length of the twist:"); // requesting the user input
  
while (!userInput.hasNextInt ()){ // while statement to check if the user entered an integer
System.out.println (" You did not enter an integer, please try again");
junk = userInput.nextLine(); // this will help in going back to the user input 
}
  
length = userInput.nextInt (); // assigning input to the length 
  
System.out.print ("Here is the output of your required Twist, ENJOY!! \n"); 
int i =1; // declaring and initializing i that will help in incrementation.
 int firstlength = length /3; 
  
  for ( i=1; i<= firstlength; i++){ // loop that will create the first line of slashes and backslashes
  System.out.print ("\\ /");
  }
  System.out.println (""); 

  
  for ( i=1; i <= firstlength ; i++){ // loop that will help in print X in the second line
  System.out.print (" X "); 
  }
  
  System.out.println ("");
  
  for ( i=1; i <= firstlength; i++){ // loop that will create the third line of slashes and backslashes
    System.out.print ("/ \\"); 
  }
System.out.println ("");
}}
  


