// Odilon Bertrand Niyomugabo 
// Lab 04, September 19, 2018 

// program that will generate a random number and it will blow  your mind 
// because it will tell you the exact card that you picked and if it is an ace or club, diamond, or spade

// command that will import and help in generating random number 
import java.util.Random; 
// opening class that is cardgenerator that will hold the whole program  
public class CardGenerator{ 
  public static void main ( String  [] args ) {
  
    Random rand = new Random (); 
  // this command will generate a new number     
      int n = rand.nextInt (52) + 1; 
// giving names to strings that will help in card naming  
    String suitname = ""; 
    String cardid = " "; 
 // if-else statements that takes one of the generated numbers
 // and decide if it shouldbe one of the card group names, diamondas, clubs, hearts or spades
if ((n >=1) && (n<=13)) {  
  suitname = "Diamonds"; 
}
    
if ((n >=14) && (n<=26)) { 
  suitname = "Clubs"; 
}
  
if ((n >=27) && (n<=39)) { 
 suitname = "Hearts"; 
    }
    
 if ((n >=40) && (n<=52)) { 
  suitname = "Spades"; 
}
    // command that will help to know what specific card is being choosen 
      int modnum = n%13;
    
// switch statement that will help in knowing the random number 
    switch (modnum) { 
        
    case 0: 
        cardid = "King";  
        break; 
        
      case 12: 
        cardid = "Queen"; 
        break; 
        
      case 11:
        cardid = "Jack";
        break; 
        
      case 1: 
        cardid = "Ace"; 
        break; 
        
      default: 
        cardid = cardid + modnum; 
        break; 
    }
    // printing out the results 
  System.out.println ("You picked the" +" "+ cardid + " "+ "of" +" " +suitname);
  } // end of main menu 
} // end of the program 
   
  
  