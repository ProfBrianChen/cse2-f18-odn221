// Odilon Bertrand 
// CSE 2 -  210 hw 10
// Program to help play a tic tac toe game 

import java.util.Scanner;
public class HW10 {
 
  public static void main(String[] args) {
        char [][] board = new char[3][3]; //create a 3*3 array
        char xcontestant = 'x'; //the x-contestant uses the char x
        char ocontestant = 'o'; //the o-contestant uses the char o
        int number = 1; //to fill in the array
        //filling the array with numbers from 1 tO 9 
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                board[i][j] = Integer.toString(number).charAt(0);
                number++;
            }
        }
       
        while (true){ //keeps executing until it breaks
            //x-contestant's turn
            if (!turn(xcontestant, ocontestant, board)) break;
            //o-contestant's trun
            if (!turn(ocontestant, xcontestant, board)) break;
        }
    }
  
    public static boolean drawSearch (char[][] list, char key) {
        //searched the entered key in the array
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++ ){
                //if found, return true
                if (key == list[i][j] && key != 'x' && key != 'o') return true;
            }  
    }
        //if not found return false
    return false;
    }
   
      public static Boolean winOutput(char [][] table){ 
    
     if ((table[0][0] == table[0][1] && table[0][0] == table[0][2]) || //horizantal one win
   (table[1][0] == table[1][1] && table[1][0] == table[1][2]) || //horizantal two win
   (table[2][0] == table[2][1] && table[2][0] == table[2][2]) || //horizantal three win 
   (table[0][0] == table[1][0] && table[0][0] == table[2][0]) || //verical one win
   (table[0][1] == table[1][1] && table[0][1] == table[2][1]) || //verical two win
   (table[0][2] == table[1][2] && table[0][2] == table[2][2]) || //verical three win
   (table[0][0] == table[1][1] && table[0][0] == table[2][2]) || //diagonal one win
   (table[2][0] == table[1][1] && table[2][0] == table[0][2])) { //diagonal two win 
   return true; 
     }
     else { 
    return false;
     }  
  }//end of the determining method   
   
    public static boolean checkDraw(char[][] array){
        //search for a number. if there is one, there are more rounds to go
        //so not a draw. But if all are x and o, and no winner, then it's a draw
        for (int number =1; number <=9; number ++){
            if(drawSearch (array, Integer.toString(number).charAt(0))) return false;
        }
        return true;
    }
   
    public static boolean turn(char contestant, char challenger, char[][] array){
        Scanner scan = new Scanner(System.in);
        char position; //refering to location in the array
        //check for winner before poceeding
        if (winOutput(array)){
          printArray(array);
            System.out.println("The " + challenger + "-contestant wins.");
            return false;//return false so that main method does not excecute anymore
        }
        //checking if there is no winner 
        else if(checkDraw(array)){
            printArray(array);
            System.out.println("The game is a draw.");
            return false; //return false so that main method does not excecute anymore
        }
        //if no win, no draw, then ask the contestant to input a position value
        else {
            printArray(array);
            System.out.println( contestant + "-contestant's turn.");
            System.out.print("Which number do you want to kick off? : ");
            char input = scan.next().charAt(0);
            //keep asking user if input in invalid
                //input is invalid if the number (representing the position is not in the array
            while(!drawSearch (array, input)){
                System.out.println("Error, try again.");
                System.out.println("Choose the position of you want to conquer: ");
                input = scan.next().charAt(0);
            }
            position = input;//position that the contestant wants to play
           
            switch(position){ //change the value at position based on input
              
                case  '1': 
                  array[0][0] = contestant; 
                break;
                case  '2': 
                  array[0][1] = contestant;
                break;
                case  '3':
                  array[0][2] = contestant;
                break;
                case  '4': 
                  array[1][0] = contestant;
                break;
                case  '5':
                  array[1][1] = contestant;
                break;
                case  '6': 
                  array[1][2] = contestant;
                break;
                case  '7': 
                  array[2][0] = contestant;
                break;
                case  '8': 
                  array[2][1] = contestant;
                break;
                case  '9': 
                  array[2][2] = contestant;
                break;
                default: break;
            }
            return true; //return true so that while keeps on going 
        }
    }
    
    
    public static void printArray(char[][] array){
   
      for(int i = 0; i< array.length; i++){
      for(int j = 0; j<array.length; j++){
        if(j == array[i].length-1) {
          System.out.print (array[i][j]);}
          else {
          System.out.print(array [i][j] + " "); 
          }}
        System.out.println();
      }}
  //end of printArray method
   
}